LOGS_FILE="../logs/logs/anon_sept-dec_known.txt"

cd code

cat $LOGS_FILE \
| node --max-old-space-size=24000 streamLines.js - --s/h/str2json \
  --s/saveReidentificationMLData "/../../data/knownlinks/expected_anon.txt" \
  --s/saveReidentificationMLData "/../../data/gw/expected_anon-idonly.txt" \
  --s/saveReidentificationMLData "" \
  --s/saveReidentificationMLData "3600000" \
  --s/saveReidentificationMLData "65536" \
  --s/saveReidentificationMLData "anon" \
  --s/saveML2CSV "anon" \
  --s/h/emptyoutput

cd ..
echo "oui,fcnt,payloadLen,devAddrDiff,spreadingFactor,datarate,fctrlack,fctrladr,tsDiff,snrDistance,espDistance,rssiDistance,gwDistance,tsDistance,linked" > "data/ml/anon.txt"
cat "data/ml/anon_linked.txt" "data/ml/anon_not-linked.txt" >> "data/ml/anon.txt"
