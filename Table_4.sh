echo "[Table_4] Parsing both files."

echo "% ----- With fcnt" > "results/Table_4.txt"
sed -n '31,44p' "results/all_results.txt" >> "results/Table_4.txt" 

echo "% ----- Without fcnt" >> "results/Table_4.txt"
sed -n '31,43p' "results/all_results_without_fcnt.txt" >> "results/Table_4.txt"
