LOGS_FILE="logs/logs/anon_sept-dec_known.txt"

cd code

echo "saveDevEUI"
cat $LOGS_FILE \
  | node streamLines.js - --s/h/str2json \
  --s/saveDevEUIDevAddrLink "anon" \
  --s/h/emptyoutput

echo "saveGWIDs"
cat $LOGS_FILE \
  | node streamLines.js - --s/h/str2json \
    --s/saveGatewayIDs "anon" \
    --s/saveGatewayIDs "all" \
    --s/h/emptyoutput

echo "saveReidentificationMLData"
cat $LOGS_FILE \
| node --max-old-space-size=24000 streamLines.js - --s/h/str2json \
  --s/saveReidentificationMLData "/../../data/knownlinks/anon.txt" \
  --s/saveReidentificationMLData "/../../data/gw/anon-idonly.txt" \
  --s/saveReidentificationMLData "" \
  --s/saveReidentificationMLData "3600000" \
  --s/saveReidentificationMLData "65536" \
  --s/saveReidentificationMLData "anon" \
  --s/saveML2CSV "anon" \
  --s/h/emptyoutput

cd ..
echo "oui,fcnt,payloadLen,devAddrDiff,spreadingFactor,datarate,fctrlack,fctrladr,tsDiff,snrDistance,espDistance,rssiDistance,gwDistance,tsDistance,linked" > "data/ml/anon.txt"
cat "data/ml/anon_linked.txt" "data/ml/anon_not-linked.txt" >> "data/ml/anon.txt"
