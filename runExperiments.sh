cd code/ml/
source venv/bin/activate

./main.py -i "../../data/ml/anon.txt" > "../../results/all_results.txt"
./main.py -i "../../data/ml/anon.txt" -e "fcnt" > "../../results/all_results_without_fcnt.txt"

#./main.py -i "../../data/ml/expected_anon.txt" > "../../results/all_results.txt"
#./main.py -i "../../data/ml/expected_anon.txt" -e "fcnt" > "../../results/all_results_without_fcnt.txt"

cd ../..

./Table_2.sh
./Table_3.sh
./Table_4.sh
