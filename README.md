Readme for reproducibility submission of paper ID 82

A) Source code info
Repository: https://gitlab.inria.fr/spelissi/wisec-2022-reproductibility
List of Programming Languages: NodeJS/Python
Packages/Libraries Needed: 
**All the packages are already installed in the VM** (ubuntu/focal64, 20.04.4). If needed anyway:
1) NodeJS (10.19.0) + npm (6.14.4)
Then run `npm install` to install all the packages found in `package.json`. 
2) python3 (3.8.10) + pip3 (20.0.2)
Then run `pip3 install -r requirements.txt` to install all the packages found in `requirements.txt`. 

B) Datasets info
DOI : [10.18709/perscido.2022.06.ds369](https://dx.doi.org/10.18709/perscido.2022.06.ds369)

C) Hardware Info
C3) Memory (size and speed) :
Generating the data requires 28Gb of RAM.
C4) Secondary Storage : 
The logs require 35Go of storage.

D) Experimentation Info
D1) VM Credentials (id:pass): vagrant:vagrant
D2) Scripts and how-tos to generate all necessary data or locate datasets:
Run `./prepareData.sh` 
In order to reduce the VM's size, the dataset is distributed separately. You need to download the dataset and mount the relevant directory (guest additions should are already be set up). See B) of this readme to download the dataset. 

By default, this script expects the logs to be at `./logs/logs.txt`. You can change this value by modifying the `LOGS_FILE` variable, line 3 of `./prepareData.sh`.

D4) Scripts and how-tos for all experiments executed for the paper:
Run `./runExperiments.sh`

The results can be found in `./results/` with a file name relative to the table in the article. 

Additional scripts (`./Table_2.sh`, `./Table_3.sh` and `./Table_4.sh`) are automatically called from `./runExperiments.sh` and are parsing `./results/all_results.txt` and/or `./results/all_results_without_fcnt.txt`. You should not need to execute them directly.

E) Software License : CC BY-NC-ND

F) Additional information
Final/expected results and (pre)processed values can be found in the `expected_[NAME].[EXTENSION]` files to help the reproduction process.

If you want to test the scripts with other random seeds, feel free to: 
1. Modify the `this.SEED` variable in `code/s/saveReidentificationMLData.js` (line 16)
2. Use the `--random` or `-r` argument for `main.py` in `./runExperiments.sh` (lines 4 and 5)
By default, `42` is used. 
