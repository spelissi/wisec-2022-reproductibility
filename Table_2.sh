echo "[Table_2] Parsing results/all_results.txt" 

echo "CLF & TP & FP & TN & FN & TPR & FPR & MCC \\ " > "results/Table_2.txt"
sed -n 57p "results/all_results.txt" >> "results/Table_2.txt"
sed -n 91p "results/all_results.txt" >> "results/Table_2.txt"
sed -n 124p "results/all_results.txt" >> "results/Table_2.txt"
sed -n 174p "results/all_results.txt" >> "results/Table_2.txt"
sed -n 211p "results/all_results.txt" >> "results/Table_2.txt"
sed -n 259p "results/all_results.txt" >> "results/Table_2.txt"
sed -n 292p "results/all_results.txt" >> "results/Table_2.txt"

echo "[Table_2] Results in results/Table_2.txt"
