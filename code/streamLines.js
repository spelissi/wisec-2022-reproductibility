/*
  Exploiting logs with chained transform streams.
*/

/**
 * @author: Samuel Pélissier, INSA Lyon, 2021
 */

const fs = require('fs')
const { hideBin } = require('yargs/helpers')

const Liner = require('./utils/liner').Liner


/*
  parsing arguments mannualy because yargs does not support arrays with "1" as
  input*.

  *: or I didn't manage to make it work.
*/
let argv = process.argv.slice(2)
let arguments = {
  '_': []
}
let isPreviousOpt = false
let previousOpt = ""
for(let a of argv) {
  if(a.startsWith('--')) {
    a = a.slice(2)
    if(!arguments[a]) {
      arguments[a] = true
    }
    previousOpt = a
    isPreviousOpt = true
  } else {
    if(isPreviousOpt) {
      if(arguments[previousOpt] == true) {
        arguments[previousOpt] = [a]
      } else {
        arguments[previousOpt].push(a)
      }
      isPreviousOpt = false
    } else {
      arguments['_'].push(a)
    }
  }
}
argv = arguments

/* Usage */
function usage(){
  console.log(argv.$0+" <incoming stream> <one or more modules>")
  console.log("The stream can either be a file or stdin ('-').")
  console.log("Example: " + argv.$0 +" - --s/log2json --s/convertHex --s/addLoraPacket --s/h/stringify")
}

if (argv._.length == 0) {
  usage();
  process.exit(1);
}


/* Streams instanciation */
var readableStream = process.stdin
const writeableStream = process.stdout
const filename = argv._[0]
if(filename !== "-") {
  readableStream = require('fs').createReadStream(filename)
}


let streams = {}
let promises = []

for(let mod of Object.keys(argv)) {
  /*
  Dynamically loading modules and adding them to the main stream
  We want each module to load processLine and to add it to the Transform stream
  Some stream require additionnal parameters, passed as options
  */
  // removing the program name from the argument list so only modules are kept
  if(mod != '_' && mod != '$0') {

    /*
    streams are added after the init() promise is resolved
    this means their order is NOT garanteed!
    thus, creating an empty object here so the promises do not screw
    the order up
    */
    streams[mod] = {}
    let params = argv[mod]

    let modInstance = require("./" + mod)
    let initMod = modInstance.initMod
    let processLine = modInstance.processLine
    let completeMod = modInstance.completeMod

    // basic options used for all cases
    let linerOpt = {
      name: mod,
      initMod: initMod,
      completeMod: completeMod,
      processLineCb: processLine,
    }

    if(typeof(params) == "boolean") {


      // the stream can be used alone (aka w/out any argument)
      if(mod.includes("log2json") || mod.includes("str2json")) {
        // the incoming data is of type string for this one
        linerOpt.writableObjectMode = false
        let l = new Liner(linerOpt)
        promises.push(l.init().then(() => {
          l.streamTransform.name = mod
          streams[mod] = l.streamTransform
        }))
      } else {
        let l = new Liner(linerOpt)
        promises.push(l.init().then(() => {
          l.streamTransform.name = mod
          streams[mod] = l.streamTransform
        }))
      }
    } else {
      // or the stream can require some arguments
      // (either at init or later, for each line)
      linerOpt.params = params
      let l = new Liner(linerOpt)
      promises.push(l.init().then(() => {
        l.streamTransform.name = mod
        streams[mod] = l.streamTransform
        // streams.push(l.streamTransform)
      }))
    }
  }
}


Promise.all(promises).then(() => {
  // now that all streams are ready, we can pipe them into the initial one
  for(let s of Object.keys(streams)) {
    readableStream = readableStream.pipe(streams[s])
  }

  /* at this point, all streams have been instanciated so we only need to pipe
    them to the final writeable stream
  */
  readableStream.pipe(writeableStream)
  // process.exit(0)
})
