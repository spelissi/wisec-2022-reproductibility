/*
  Save ML data to CSV files

  The await/async syntax should be easier to read but SOMEHOW it does not work.
*/
const fs = require('fs')

const files = require('../utils/files')


function initMod(params) {
  this.outputsuffix = params[0] ? params[0] : ""

  let outputLinkedFilename = __dirname + "/../../data/ml/" + this.outputsuffix + "_linked.txt"
  let outputNotLinkedFilename = __dirname + "/../../data/ml/" + this.outputsuffix + "_not-linked.txt"

  console.log("Saving data to:")
  console.log("- " + outputLinkedFilename)
  console.log("- " + outputNotLinkedFilename)

  this.outputLinked = fs.createWriteStream(outputLinkedFilename, {flags: 'a'})
  this.outputNotLinked = fs.createWriteStream(outputNotLinkedFilename, {flags: 'a'})
}


function processLine(entry, params) {

  return new Promise((resolve, reject) => {
    if(entry) {
      let promises = []
      if(Array.isArray(entry) && entry.length != 0) {
        for(let obj of entry) {
          if(obj) {
            promises.push(files.writeLines(obj.linked.join(""), this.outputLinked))
            promises.push(files.writeLines(obj.notLinked.join(""), this.outputNotLinked))
            process.exit(0)
          }
        }
      } else if(entry.linked) {
        promises.push(files.writeLines(entry.linked.join(""), this.outputLinked))
        promises.push(files.writeLines(entry.notLinked.join(""), this.outputNotLinked))
      }

      Promise.all(promises).then(() => {
        resolve()
      })
    } else {
      resolve()
    }

  })
}


function completeMod() {
  this.outputLinked.close()
  this.outputNotLinked.close()
}


module.exports = {
  initMod,
  processLine,
  completeMod
}
