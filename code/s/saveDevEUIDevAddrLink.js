/*
  Using Application/*: saving the official correspondance between DevEUI
  and DevAddr
*/
const fs = require('fs')


function initMod(params) {
  // file suffix
  this.outputsuffix = params[0]
  // the list of gateways theorically controlled
  // -> limits saved knownlinks
  this.controlledGateways = params[1] ? params[1].split(',') : undefined
  // a time range to select a subset of the entries
  this.minTs = params[2] ? Number(params[2]) : 0
  this.maxTs = params[3] ? Number(params[3]) : 20000000000000000 // if someone still uses this code in 2603, idk what to tell you

  this.devList = {}
  this.entryCount = 0
}


function checkGateways(entry) {
  for(let r of entry.rxInfo) {
    if(this.controlledGateways.includes(r.gatewayID)) {
      return true
    }
  }
  return false
}


function processLine(entry) {
  this.entryCount++

  if(this.entryCount % 1000 == 0) {
    console.log(this.entryCount)
  }

  // only treating packets with a DevAddr
  if(entry.devEUI && entry.devAddr) {
    let devEUI = entry.devEUI
    let devAddr = entry.devAddr
    if(this.controlledGateways) {
      if(!checkGateways.bind(this)(entry)) {
        return entry
      }
    }

    let ts = entry._timestamp
    if(!(ts > this.minTs && ts < this.maxTs)) {
      return entry
    }

    let gatewayList = []
    for(let gw of entry.rxInfo) {
      gatewayList.push(gw.gatewayID)
    }

    if(this.devList[devEUI]) {
      this.devList[devEUI].push({devAddr: devAddr, timestamp: ts, gw: gatewayList})
    } else {
      this.devList[devEUI] = [{devAddr: devAddr, timestamp: ts, gw: gatewayList}]
    }
  }
  // by default, returning all entry
  // return entry
}


function completeMod() {
  // let filename = __dirname + "/../output/knownlinks/" + this.outputsuffix + "_" + Date.now() + ".txt"
  let filename = __dirname + "/../../data/knownlinks/" + this.outputsuffix + ".txt"

  console.log("Writing data in ", filename)
  const output = fs.createWriteStream(filename)

  for(let [key, value] of Object.entries(this.devList)) {
      output.write(JSON.stringify({devEUI: key, devAddresses: value}) + "\n")
  }
  output.close()
}


module.exports = {
  initMod,
  processLine,
  completeMod
}
