/*
  Preparing data to an intelligible format for binary classification algorithms
*/
const fs = require('fs')
const readline = require('readline')

const vector = require('../utils/vector')
const scoreFx = require('../utils/computeScore')
const files = require('../utils/files')

const seedrandom = require('seedrandom')


function initMod(params) {
  // PRNG seeding for reproductibility
  this.SEED = "42"
  this.prng = new seedrandom(this.SEED)
  /* Parameters */
  // path to a file with a list of known addresses associations
  this.knownAddressesFile = params[0]
  // path to a file containing a list of gateways
  // (aka the one expected in the logs)
  this.gwFile = params[1]

  // using the name of the file to get the function.
  // may be kind of limited but works for now :/
  this.scoreFunction = params[2] ? params[2] : "allSum"

  // the maximum time waited after a devEUI before looking for associations
  this.timeThreshold = params[3] ? Number(params[3]) : 86400000

  // maximum value of frame counter to add an uplink to this.uplinkMessage
  // yes, this is optimisation.
  this.maxFcnt = params[4] ? Number(params[4]) : 0

  // suffixes used for the output file containing the results
  this.outputsuffix = params[5] ? params[5] : params[2]

  // a time range to select a subset of the entries
  this.minTs = params[6] ? Number(params[6]) : 0
  this.maxTs = params[7] ? Number(params[7]) : 20000000000000 // if someone still uses this code in 2603, idk what to tell you

  // if this boolean is true, the time range is supposed to be excluded
  this.exclude = params[8] == 'exclude'

  // the list of gateways theorically controlled
  this.controlledGateways = params[9] ? params[9].split(',') : undefined

  this.timeWindow = 10000

  /* Dynamic values */
  this.joinRequests = {} // a dict of join requests

  this.knownLinks = {} // a dict of dicts of known uplinks (by devEUI and phyPayload)
  this.notLinked = {} // a dict of dicts of unkown uplinks (by devEUI and phyPayload)

  this.previousDevAddr = {} // a dict of devAddr with the last timestamp they were found

  this.entryCount = 0

  this.beforeJoinreq = 0
  this.afterJoinreq = 0

  let outputLinkedFilename = __dirname + "/../../data/ml/" + this.outputsuffix + "_linked.txt"
  let outputNotLinkedFilename = __dirname + "/../../data/ml/" + this.outputsuffix + "_not-linked.txt"

  console.log("Saving data to:")
  console.log("- " + outputLinkedFilename)
  console.log("- " + outputNotLinkedFilename)
  this.outputLinked = fs.createWriteStream(outputLinkedFilename, {flags: 'a'})
  this.outputNotLinked = fs.createWriteStream(outputNotLinkedFilename, {flags: 'a'})

  let promises = []
  /*
    Reading a reference file containing the known links between
    devEUI and devAddr(esses). Using some Promise magic
  */
  promises.push(new Promise((resolve, reject) => {
    let inFileName = __dirname + this.knownAddressesFile
    this.input = fs.createReadStream(inFileName)
    console.log("Reading data from ", inFileName)

    // the file is not supposed to be too big to load in memory
    const rl = readline.createInterface({
        input: fs.createReadStream(inFileName),
        output: process.stdout,
        terminal: false
    })

    let readAddresses = []
    rl.on('line', (line) => {
      readAddresses.push(JSON.parse(line))
    })

    rl.on('close', () => {
      this.knownAddresses = {}
      for(let ra of readAddresses) {
        this.knownAddresses[ra.devEUI] = ra.devAddresses
      }
      console.log("Known addresses read from file, resolving.")
      resolve()
    })
  }))

  /*
    Reading a reference file containing the list of all known gateways
  */
  promises.push(new Promise((resolve, reject) => {
    let inFileName = __dirname + this.gwFile
    this.input = fs.createReadStream(inFileName)
    console.log("Reading data from ", inFileName)

    // the file is not supposed to be too big to load in memory
    const rl = readline.createInterface({
        input: fs.createReadStream(inFileName),
        output: process.stdout,
        terminal: false
    })

    this.gateways = []
    rl.on('line', (line) => {
      this.gateways.push(line)
    })

    rl.on('close', () => {
      // the combination takes into account the lonely gateways
      this.gatewayCombinations = this.gateways.concat(this.gateways.flatMap(
        (v, i) => this.gateways.slice(i+1).map( w => v + '_' + w )
      ))
      console.log("Gateway list read from file, resolving.")
      resolve()
    })
  }))

  // only resolving when all promises are resolved themselves
  return new Promise((resolve, reject) => {
    Promise.all(promises).then(() => {
      console.log("All promises resolved.")
      resolve()
    })
  })
}


function isKnownLink(joinEntry, upEntry) {
  /*
    1. DevEUI and DevAddr are known
    2. The timestamps of the uplink message are close enough to the expected one
  */
  if(this.knownAddresses[joinEntry._packet.deveui]) {
    let upTs = upEntry._timestamp
    for(let a of this.knownAddresses[joinEntry._packet.deveui]) {
      if(a.devAddr == upEntry._packet.devaddr) {
        if(Math.abs(a.timestamp - upTs) < this.timeWindow) {
          return true
        }
        // at this point, we found the correct association but the timer is wrong
        // this means we can quit early
        return false
      }
    }
  }
  return false
}


function convertToCSV(obj) {
  /*
    We *could* save the entry as a filthy JSON sintrigified object
    -> file.write(JSON.stringify(entry) + '\n')
    But that would be harder to play with later. Thus, converting only the
    necessary data into a comma separated CSV line
  */
  let res = ""
  // res += obj.joinEntry._packet.deveui + ","
  // res += obj.uplink._packet.devaddr + ","

  // splitting the deveui to get the OUI, then converting it to int
  res += parseInt(obj.joinEntry._packet.deveui.slice(0, 6), 16)
  res += "," + obj.uplink._packet.fcnt
  res += "," + obj.uplink._packet.payloadLen
  res += "," + obj.uplink.devAddrFreshness

  if(obj.uplink.txInfo.loRaModulationInfo.spreadingFactor) {
    res += "," + obj.uplink.txInfo.loRaModulationInfo.spreadingFactor
  } else {
    res += ",0"
  }

  if(obj.uplink.txInfo._datarate) {
    res += "," + obj.uplink.txInfo._datarate
  } else {
    res += ",-1"
  }

  for(let [key, val] of Object.entries(obj.uplink['addedValues'])) {
    res += "," + val
  }

  res += "," + obj.linked
  if(res.charAt(0) == ',') {
    console.log("obj", obj)
    console.log("res", res)
    process.exit(0)
  }

  return res + "\n"
}


function setupVectors(entry, addr) {
  /*
    Helper setting up all the vectors for a specified entry
  */
  let snrVector = new vector.Vector(this.gateways.length, addr + "_SNR", -26)
  let espVector = new vector.Vector(this.gateways.length, addr + "_ESP", -150)
  let rssiVector = new vector.Vector(this.gateways.length, addr + "_RSSI", -135)
  let gwVector = new vector.Vector(this.gateways.length, addr + "_gw")
  let timestampVector = new vector.TimestampVector(this.gatewayCombinations.length, addr + "_ts")

  snrVector.initByGw(entry.gw, this.gateways, "loRaSNR")
  espVector.initByGw(entry.gw, this.gateways, "esp")
  rssiVector.initByGw(entry.gw, this.gateways, "rssi")
  gwVector.initByGw(entry.gw, this.gateways, "gw")

  timestampVector.initByGwCombination(entry.gw, this.gatewayCombinations, "timestamp")

  return [snrVector, espVector, rssiVector, gwVector, timestampVector]
}


function updateEntry(entry, refEntry, vectors) {
  /*
    We want to enrich the uplink entry with various data:
    - differences between join and up (eg first timestamp)
    - euclidean distances (ESP, RSSI, timestamps)
  */
  let [refSNRVector, refESPVector, refRSSIVector, refGwVector, refTimestampVector] = vectors
  let [upSNRVector, upESPVector, upRSSIVector, upGwVector, upTimestampVector] = setupVectors.bind(this)(entry, entry._packet.devaddr)

  entry['addedValues'] = {}

  // booleans
  entry['addedValues']['fctrlack']  = entry.rxInfo.fctrlack == refEntry.txInfo.fctrlack ? 1 : 0
  entry['addedValues']['fctrladr']  = entry.rxInfo.fctrladr == refEntry.txInfo.fctrladr ? 1 : 0

  // differences
  entry['addedValues']['timestampDiff'] = entry._timestamp - refEntry._timestamp

  // distances
  entry['addedValues']['snrDistance'] = refSNRVector.eucDistance(upSNRVector)
  entry['addedValues']['espDistance'] = refESPVector.eucDistance(upESPVector)
  entry['addedValues']['rssiDistance'] = refRSSIVector.eucDistance(upRSSIVector)
  entry['addedValues']['gwDistance'] = refGwVector.eucDistance(upGwVector)

  entry['addedValues']['timestampDistance'] = refTimestampVector.eucDistance(upTimestampVector)

  for(let [key, val] of Object.entries(entry['addedValues'])) {
    if(isNaN(val)) {
      console.log("[NaN] key:", key)
      console.log("---")
      console.log("entry", entry)
      console.log("---")
      console.log("refEntry", refEntry)
      process.exit(0)
    }
  }

  // final score
  // entry['addedValues']['score'] = scoreFx[this.scoreFunction].bind(this)(entry['addedValues'])
  return entry
}


function saveData(joinEntry) {
  /*
    We want to save the known link for the current devEUI
    and then save at max 10 random not linked to the current devEUI

    Note: it's possible no uplink message correspond to the join request
  */

  let results = {
    linked: [],
    notLinked: []
  }

  let joinDevEUI = joinEntry._packet.deveui

  if(!this.notLinked[joinDevEUI] && !this.knownLinks[joinDevEUI]) {
    //console.log("No uplink message found for this devEUI")
    return
  }

  let vectors = setupVectors.bind(this)(joinEntry, joinDevEUI)

  // it's possible there are multiple candidates for known links, as it's only
  // a correspondance of addresses and timestamps
  let bestLink
  if(this.knownLinks[joinDevEUI]) {
    if(Object.keys(this.knownLinks[joinDevEUI]).length > 1) {
      // if so, we keep the closest timestamp to the known one
      let smallestTimeDiff
      for(let [phyPayload, knownUp] of Object.entries(this.knownLinks[joinDevEUI])) {
        for(let a of this.knownAddresses[joinDevEUI]) {
          if(a.devAddr == knownUp._packet.devaddr) {
            let timeDiff = Math.abs(a.timestamp - knownUp._timestamp)
            if(smallestTimeDiff) {
              if(timeDiff < smallestTimeDiff) {
                smallestTimeDiff = timeDiff
                bestLink = knownUp
              }
            } else {
              smallestTimeDiff = timeDiff
              bestLink = knownUp
            }
          }
        }
      } // first for loop

      // now that we know which uplink is the bestLink, we can write the others
      // in the not linked file
      for(let [phyPayload, upLink] of Object.entries(this.knownLinks[joinDevEUI])) {
        if(phyPayload != bestLink.phyPayload) {
          upLink = updateEntry.bind(this)(upLink, joinEntry, vectors)
          let obj = {
            joinEntry: joinEntry,
            uplink: upLink,
            linked: 0,
          }
          results.notLinked.push(convertToCSV(obj))
        }
      }
    } else {
      bestLink = this.knownLinks[joinDevEUI][Object.keys(this.knownLinks[joinDevEUI])[0]]
    }
    bestLink = updateEntry.bind(this)(bestLink, joinEntry, vectors)
  }

  /* Adding the link and score to the join request */
  if(bestLink) {
    let obj = {
      joinEntry: joinEntry,
      uplink: bestLink,
      linked: 1,
    }
    results.linked.push(convertToCSV(obj))
  }

  // now, do it again with (max) 10 random bad links :)
  if(this.notLinked[joinDevEUI]) {
    let nbNotLinked = Object.keys(this.notLinked[joinDevEUI]).length
    let notLinkedEntries = []
    // if there are more than 10 uplink, selecting a list of 10 random ones
    if(nbNotLinked > 10) {
      var arr = [];
      while(arr.length < 10){
          let r = Math.floor(this.prng() * nbNotLinked) + 1
          if(arr.indexOf(r) === -1) arr.push(r)
      }

      for(let i in arr) {
        notLinkedEntries.push(this.notLinked[joinDevEUI][Object.keys(this.notLinked[joinDevEUI])[i]])
      }
    } else {
      // in case there are not enough entries, putting everything
      notLinkedEntries = Object.values(this.notLinked[joinDevEUI])
    }

    for(let notLinked of notLinkedEntries) {
      notLinked = updateEntry.bind(this)(notLinked, joinEntry, vectors)
      let obj = {
        joinEntry: joinEntry,
        uplink: notLinked,
        linked: 0,
      }
      results.notLinked.push(convertToCSV(obj))
    }
  }

  /* We've saved everything, now it's time for some CLEAN UP BABY */
  cleanUp.bind(this)(joinDevEUI)
  return results
}


function cleanUp(joinDevEUI) {
  delete this.joinRequests[joinDevEUI]
  delete this.knownLinks[joinDevEUI]
  delete this.notLinked[joinDevEUI]
}


function updateGateway(gatewayID, baseEntry, valuesEntry) {
  /*
    Helper updating the gateways-related values from a valuesEntry into a baseEntry
  */
  baseEntry.gw[gatewayID] = {
    timestamp: valuesEntry._timestamp,
    rssi: valuesEntry.rxInfo.rssi,
    esp: valuesEntry.rxInfo._esp,
    loRaSNR: valuesEntry.rxInfo.loRaSNR,
    channel: valuesEntry.rxInfo.channel,
    antenna: valuesEntry.rxInfo.antenna,
  }
  return baseEntry
}


function processLine(entry) {
  let results = []

  if(this.entryCount % 5000 == 0) {
    console.log("++++++++++++++")
    console.log("[-] Number of entries treated:", this.entryCount)
    console.log("[-] Number of join requests currently saved", Object.keys(this.joinRequests).length)
    console.log("")
  }
  this.entryCount++

  if(!entry) {
    return results
  }

  let ts = entry._timestamp
  if(this.exclude) {
    if(ts >= this.minTs && ts <= this.maxTs) {
      return results
    }
  } else {
    if(!(ts >= this.minTs && ts <= this.maxTs)) {
      return results
    }
  }

  let pkt = entry._packet
  if(pkt) {
    let mtype = pkt.mtype
    let gatewayID
    if(entry.gatewayID) {
      gatewayID = entry.gatewayID
    } else if(entry.rxInfo && entry.rxInfo.gatewayID) {
      gatewayID = entry.rxInfo.gatewayID
    } else {
      // last solution: spliting stuff
      gatewayID = entry._topic.split("/")[1]
    }
    // console.log("------------------------------------------------------------")
    // console.log("[processLine]")
    // console.log("- gatewayID", gatewayID)
    // console.log("- devEUI", entry._packet.deveui)
    // console.log("- devAddr", entry._packet.devaddr)
    // console.log("- this.joinRequests", this.joinRequests)
    // console.log("- mtype", mtype)

    /*
      If the list of controlled gateways has been used, we check that the
      current entry is relevant. If not, insta return!
      So passed this point, we're sure to work with controlled entries
    */
    if(this.controlledGateways) {
      if(!(this.controlledGateways.includes(gatewayID))) {
        return results
      }
    }

    if(mtype == "Join Request" && !entry._topic.startsWith('application')) {
      /* Saving the join request to be examided later */
      let isNew = true

      for(let [key, joinEntry] of Object.entries(this.joinRequests)) {
        if(entry.phyPayload == joinEntry.phyPayload) {
          // only adding new elements
          joinEntry = updateGateway(gatewayID, joinEntry, entry)
          isNew = false
        }
      }

      if(isNew) {
        // console.log("Adding new join request for devEUI", entry._packet.deveui)

        /*
          In order to reduce the number join requests currently held:
          when a new join request with a devEUI already saved is found,
          we save the corresponding data

          A simple clean is WRONG, as there could be:
          - devEUI1
          - devAddrA
          - devEUI1
          - devAddrB
          which represent 2 different links!
          (devEUI1-devAddrA and devEUI1-devAddrB)
        */
        if(this.joinRequests[entry._packet.deveui]) {
          results.push(saveData.bind(this)(this.joinRequests[entry._packet.deveui]))
        }
        /*
          We still need to save the incoming entry, tho :)
        */
        entry.gw = {}
        // gateway specifics
        entry = updateGateway(gatewayID, entry, entry)
        this.joinRequests[entry._packet.deveui] = entry
      } // is New
    } else if(mtype.endsWith("Data Up") && !entry._topic.endsWith('down')) {
      // sometimes, the topic and the mtype are contrary (yes.)

      // If the joinRequests queue is empty, this uplink message is useless
      // (as there is nothing to compare it to)
      if(Object.keys(this.joinRequests).length != 0) {
        // console.log("[processLine] Adding new uplink message for devAddr", devAddr)
        /*
          We want to group uplinkMessages with possible joinRequests
          so for each joinRequests inside the queue, we add the uplink message
        */
        for(let [key, joinEntry] of Object.entries(this.joinRequests)) {
          /*
            If the time difference between the uplink message and the
            join request has exceeded the threshold, it means we can work find the
            optimal candidate inside the queue
          */
          let timeThresholdExceeded = false
          let e_timestamp = entry._timestamp
          for(let jv of Object.values(joinEntry.gw)) {
            // console.log("e_timestamp - jv.timestamp", e_timestamp - jv.timestamp)
            // console.log("e_timestamp - jv.timestamp < this.timeThreshold", e_timestamp - jv.timestamp < this.timeThreshold)
            if((e_timestamp - jv.timestamp) > this.timeThreshold) {
              // note: as there are multiple timestamps (one for each gateway),
              // only proceed when every single one of them is above the threhold
              timeThresholdExceeded = true
              break
            }
          }

          if(timeThresholdExceeded) {
            console.log("[processLine] Time threshold exceeded. Saving some data!")
            results.push(saveData.bind(this)(joinEntry))
          }

          /*
            If the elapsed time is not sufficient, we keep saving up new
            uplink messages
          */
          let isNew = true
          let knownLink = isKnownLink.bind(this)(joinEntry, entry)
          // it is still possible that the uplink message is duplicated on
          // multiple gateways
          if(knownLink) {
            if(this.knownLinks[joinEntry._packet.deveui]) {
              if(this.knownLinks[joinEntry._packet.deveui][entry.phyPayload]) {
                this.knownLinks[joinEntry._packet.deveui][entry.phyPayload] = updateGateway(gatewayID, this.knownLinks[joinEntry._packet.deveui][entry.phyPayload], entry)
                isNew = false
              }
            }
          } else {
            if(this.notLinked[joinEntry._packet.deveui]) {
              if(this.notLinked[joinEntry._packet.deveui][entry.phyPayload]) {
                this.notLinked[joinEntry._packet.deveui][entry.phyPayload] = updateGateway(gatewayID, this.notLinked[joinEntry._packet.deveui][entry.phyPayload], entry)
                isNew = false
              }
            }
          }

          if(isNew) {
            if(entry._packet.fcnt <= this.maxFcnt) {
              entry.gw = {}
              // gateway specifics
              entry = updateGateway(gatewayID, entry, entry)

              if(knownLink) {
                // add to knownLinks
                if(!this.knownLinks[joinEntry._packet.deveui]) {
                  this.knownLinks[joinEntry._packet.deveui] = {}
                }
                this.knownLinks[joinEntry._packet.deveui][entry.phyPayload] = entry
              } else {
                // add to notLinked
                if(!this.notLinked[joinEntry._packet.deveui]) {
                  this.notLinked[joinEntry._packet.deveui] = {}
                }
                this.notLinked[joinEntry._packet.deveui][entry.phyPayload] = entry
              }
            } // maxFcnt

            /*
            devAddr freshness
            - If the no previous timestamp is found, the current link has to be a first link.
            - If the previous timestamp observed is between the join req and the current uplink, it is *impossible* to be the first link. Like, at all.
            - If the previous timestamp is *before* the join req, there is a chance a (re-)join has occured
            So the difference between timestamps should not be done between the current timestamp and the previous one, but with the one on the join request!
            */
            if(this.previousDevAddr[entry._packet.devaddr]) {
              let withJoinReq = joinEntry._timestamp - this.previousDevAddr[entry._packet.devaddr]

              if(withJoinReq < 0) {
                // this means the previous occurrence is found after the joinreq
                entry.devAddrFreshness = - this.prng()
                this.afterJoinreq++
              } else {
                // this is always positive
                entry.devAddrFreshness = entry._timestamp - this.previousDevAddr[entry._packet.devaddr]
                this.beforeJoinreq++
              }
            } else {
              entry.devAddrFreshness = 0
            }
            // do not forget to update the timestamp
            this.previousDevAddr[entry._packet.devaddr] = entry._timestamp
          } // isNew
        } // end of for loop
      }
    }
  }
  return results
}


function completeMod() {
  /*
    At this point, there are still messages saved in the buffers but they will
    never be saved as the processLine function is not called anymore.
    Thus, we need to do it manually.
  */
  console.log("Completed stream, saving leftovers...")
  let results = []
  let count = 0
  for(let [key, joinEntry] of Object.entries(this.joinRequests)) {
    results.push(saveData.bind(this)(joinEntry))
    count++
  }

  let promises = []
  if(results.length != 0) {
    for(let obj of results) {
      if(obj) {
        promises.push(files.writeLines(obj.linked.join(), this.outputLinked))
        promises.push(files.writeLines(obj.notLinked.join(), this.outputNotLinked))
      }
    }
  }

  Promise.all(promises).then(() => {
    console.log("All promises resolved.")
    console.log("Number of left overs:", count)

    this.outputLinked.close()
    this.outputNotLinked.close()
  })
}


module.exports = {
  initMod,
  processLine,
  completeMod,
}
