/*
  Convert a string directly into JSON
*/
function processLine(line){
  return JSON.parse(line)
}

module.exports = {
  processLine
}
