/*
  Print the JSON line as a string in the standard output
*/

function processLine(line){
  if(line) {
    return JSON.stringify(line) + '\n'
  } else {
    return ""
  }
}

module.exports = {
  processLine
}
