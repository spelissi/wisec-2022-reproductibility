/*
 Do not print anything on the standard output (useful to increase performance by bypassing the costly print operation)
*/
function processLine(line){
  return ""
}

module.exports = {
  processLine
}
