/*
  Saving a list of all unqiues gatewayID found in the logs
*/
const fs = require('fs')


function initMod(params) {
  this.outputsuffix = params[0]
  this.saveMode = params[1]

  this.nbFilename = __dirname + "/../../data/gw/" + this.outputsuffix + "-nb" + ".txt"
  console.log("Saving gateways IDs (with numbers) in", this.nbFilename)
  this.nbOutput = fs.createWriteStream(this.nbFilename)
  if(this.saveMode == "all") {
    this.idOnlyFilename = __dirname + "/../../data/gw/" + this.outputsuffix + "-idonly" + ".txt"
    console.log("Saving gateways IDs (only) in", this.idOnlyFilename)
    this.idOnlyOutput = fs.createWriteStream(this.idOnlyFilename)
  }

  this.gatewayList = {}
  this.entryCount = 0
}


function processLine(entry) {
  if(this.entryCount % 5000 == 0) {
    console.log("++++++++++++++")
    console.log("[-] Number of entries treated:", this.entryCount)
    console.log("")
  }
  this.entryCount++

  if(entry.rxInfo) {
    let gatewayId = entry.rxInfo.gatewayID
    if(!this.gatewayList[gatewayId]) {
      this.gatewayList[gatewayId] = 1
    } else {
      this.gatewayList[gatewayId]++
    }
  }

  // by default, returning all entry
  return entry
}


function completeMod() {

  for(let [key, value] of Object.entries(this.gatewayList)) {
    let obj = {}
    obj[key] = value
    this.nbOutput.write(JSON.stringify(obj) + "\n")
    if(this.saveMode == "all") {
      this.idOnlyOutput.write(key + "\n")
    }
  }

  // do not forget to close the file descriptor
  this.nbOutput.close()
  if(this.saveMode == "all") {
    this.idOnlyOutput.close()
  }
}


module.exports = {
  initMod,
  processLine,
  completeMod
}
