/*
  Implementation of a transform stream reading/writing line by line
  as a basic module


  Based on:
  https://strongloop.com/strongblog/practical-examples-of-the-new-node-js-streams-api/
  https://medium.com/florence-development/working-with-node-js-stream-api-60c12437a1be


  Minimal example (prints line on stdout):
  const fs = require('fs')
  const Liner = require('./liner').Liner

  function processline(line){
    // do other operations on each line here
    return line
  }

  let l = new Liner(processline)

  const source = process.stdin
                .pipe(l.streamTransform)
                .pipe(process.stdout)

  For more information about developing transform streams for this project, see:
  streams_interface.md
*/
var stream = require('stream')

class Liner {
  constructor({name="", initMod, completeMod, processLineCb, params={}, readableObjectMode=true, writableObjectMode=true}) {
    /*
    name: cool helper for debugging purposes, to know which transform stream is
         currently broken
    initMod: function called at the start of the initialisation
    completeMod: function called after the stream's "finish" event
    processLineCb: callback function used to transform one line (string or obj)
                  one each time it's called
    params : parameters used either for the module initiation or the call of
            processLineCb
    readableObjectMode/writableObjectMode: options to create the Transform stream
    */

    // console.log("New liner", name, initMod, completeMod, processLineCb, params, readableObjectMode, writableObjectMode)

    this.name = name
    this.initMod = initMod
    this.completeMod = completeMod
    this.processLineCb = processLineCb
    this.params = params
    this.readableObjectMode = readableObjectMode
    this.writableObjectMode = writableObjectMode
    // this.setupStr()
  }

  async init() {
    return new Promise(resolve => {
      if(this.initMod) {
        // the initMod function can return a promise itself
        // or it can be a simple synchronous function
        let r = this.initMod(this.params)
        if(r != undefined) {
          r.then(() => {
            this.setupStr()
            resolve()
          })
        } else {
          this.setupStr()
          resolve()
        }
      } else {
        // it's possible no initMod function has been used
        this.setupStr()
        resolve()
      }
    })
  }

  setupStr() {
    this.streamTransform = new stream.Transform({
        readableObjectMode: this.readableObjectMode,
        writableObjectMode: this.writableObjectMode,
    })

    // saving this context before losing it in the following functions
    var self = this

    this.streamTransform._transform = function (data, encoding, done) {
      /* the buffer can either be:
      - a Buffer (aka a part of a string)
      - a JS/JSON object
      - an Array
      */

      if(data === undefined) {
        done()
        return
      }

      if(Buffer.isBuffer(data)) {
        var data = data.toString()
        if (self._lastLineData) data = self._lastLineData + data
        let lines = data.split('\n')
        self._lastLineData = lines.splice(lines.length-1,1)[0]
        let new_lines = []
        for(let l of lines) {
          // applying the callback on every line
          let nl = self.processLineCb(l, self.params)
          new_lines.push(nl)
        }
        lines = new_lines
        lines.forEach(this.push.bind(this))

        done()
      } else if(Array.isArray(data)) {
        let promises = []
        for(let d of data) {
          let res = self.processLineCb(d, self.params)
          // processLine can either be a classic function returning a JSON object
          // OR an async one, returning a Promise
          // done() can only be called when *all* the promises are resolved
          if(res && typeof res.then == 'function') {
            promises.push(res)
            res.then((val) => {
              // even if we do not call done() yet, we still need to push
              // the value
              this.push(val)
            })
          } else {
            // not a promise? perfect, just push
            this.push(res)
            done()
          }
        }

        if(promises.length > 0) {
          Promise.all(promises).then(() => {
            done() // then, and only then, can we call done() (houray)
          })
        } else {
          done()
        }
      } else {
        // in case of JSON object, the treatment can be done directly
        let res = self.processLineCb(data, self.params)

        // processLine can either be a classic function returning a JSON object
        // OR an async one, returning a Promise
        if(res && typeof res.then == 'function') {
          res.then((val) => {
            this.push(val)
            done() // only calling done() when the promise is resolved
          })
        } else {
          this.push(res)
          done()
        }
      }
    }

    this.streamTransform._flush = function (done) {
         if (self._lastLineData) self.push(self._lastLineData)
         self._lastLineData = null
         done()
    }

    this.streamTransform.on('finish', () => {
      if(self.completeMod) {
        self.completeMod()
      }
    })
  }
}

module.exports = {
  Liner
}
