/*
  Functions used to compute the score of a specific entry
  used with linkDevEUIDevAddr.js
*/

function allSum(entry) {
  return entry['espDistance'] / this.maxEspDistance + entry['rssiDistance'] / this.maxRssiDistance
  + entry['timestampDistance'] / this.maxTimestampDistance + entry['timestampDiff'] / this.maxTimestampDiff
}


function sumWithoutRssi(entry) {
  return entry['espDistance'] / this.maxEspDistance
  + entry['timestampDistance'] / this.maxTimestampDistance + entry['timestampDiff'] / this.maxTimestampDiff
}


function sumWithFcnt(entry) {
  let sum = entry['espDistance'] / this.maxEspDistance
  + entry['timestampDistance'] / this.maxTimestampDistance + entry['timestampDiff'] / this.maxTimestampDiff
  // adding a ratio from the fcnt
  let fcnt = entry._packet.fcnt
  if(fcnt == 0) {
    fcnt = 0.5
  }

  return sum * 1/fcnt
}


function espDistanceOnly(entry) {
  return entry['espDistance'] / this.maxEspDistance
}


function rssiDistanceOnly(entry) {
  return entry['rssiDistance'] / this.maxRssiDistance
}


function timestampDistanceOnly(entry) {
  return entry['timestampDistance'] / this.maxTimestampDistance
}


function timestampDiffOnly(entry) {
  return entry['timestampDiff'] / this.maxTimestampDiff
}


function hammingDistanceOnly(entry) {
  return entry['hammingDistance']
}


function timestampOnly(entry) {
  return entry._timestamp
}


module.exports = {
  allSum,
  sumWithoutRssi,
  sumWithFcnt,
  espDistanceOnly,
  rssiDistanceOnly,
  timestampDistanceOnly,
  timestampDiffOnly,
  timestampOnly,
  hammingDistanceOnly
}
