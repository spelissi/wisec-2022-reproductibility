function writeLines(content, file) {
  /*
    params:
    - content: a string (single line, multiple lines, whatever)
    - file: the WriteStream descriptor

    returns:
      - a Promise resolving when the writing action is finished
  */
  return new Promise((resolve, reject) => {
    file.write(content, () => {
        resolve()
    })
  })
}


module.exports = {
  writeLines
}
