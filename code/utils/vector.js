function combineGw(gw) {
  /*
    Create a combination of the gatewayIDs contained in the gw object

    params:
      - gw: an object of the following format:
    {
      "gatewayId1": {...},
      "gatewayId2": {...},
      "gatewayIdN": {...}
    }

    return:
      an array of arrays
      [[gw1, gw2'], [gw1, gw3], [gw2, gw3]]
      [[gw1]]
  */
  // recreating an array of gatewayIDs
  let array = []
  for(let gatewayID of Object.keys(gw)) {
    array.push(gatewayID)
  }

  // it's possible there is only 1 gateway
  if(array.length == 1) {
    return [array]
  }

  // combining IDs to get all possibilities
  let combined = array.flatMap(
      (v, i) => array.slice(i+1).map( w => [v, w] )
  )
  return combined
}


function getCombinedValues(gw, combined, type) {
  /*
    Create an array of the differences between values of the gw object

    params:
      - gw: gateways object
      - combined: a combination of gateway IDs (see combineGw)
    return:
      an array of results
  */
  let arr = []
  for(let [gw1, gw2] of combined) {
    let res = {}
    // it's possible there is only 1 gateway
    if(gw2) {
      res.id = gw1 + '_' +gw2
      res[type] = Math.abs(gw[gw1][type] - gw[gw2][type])
    } else {
      res.id = gw1
      res[type] = Math.abs(gw[gw1][type])
    }
    arr.push(res)
  }
  return arr
}


/* --------------------------------------- */
class Vector {
  constructor(length, name, defaultValue=0) {
    this.length = length
    this.name = name // debugging purposes
    this.init(defaultValue)
  }

  init(defaultValue) {
    // setting the array to zeros
    this.arr = new Array(this.length)
    for (let i=0; i<this.length; ++i) {
      this.arr[i] = defaultValue
    }
  }

  initByGw(gatewayObj, gatewayList, type) {
    if(type == "gw") {
      for(let [key, value] of Object.entries(gatewayObj)) {
        this.setValues(gatewayList.indexOf(key), [1])
      }
    } else {
      for(let [key, value] of Object.entries(gatewayObj)) {
        this.setValues(gatewayList.indexOf(key), [value[type]])
      }
    }
    // console.log("["+ this.name +"][initGw] this.arr after setting values", this.arr)
  }

  initByGwCombination(gatewayObj, gatewayList, type) {
    //console.log("["+ this.name +"]gatewayObj", gatewayObj)
    //console.log("["+ this.name +"]gatewayList", gatewayList)
    let combinedValues = getCombinedValues(gatewayObj, combineGw(gatewayObj), type)
    //console.log("["+ this.name +"]combinedValues", combinedValues)
    for(let v of combinedValues) {
      this.setValues(gatewayList.indexOf(v.id), [v.timestamp])
    }
  }

  setValues(startIndex, values) {
    //console.log("["+ this.name +"][setValues]", startIndex, values)
    if(startIndex + values.length > this.length) {
      //console.error("["+ this.name +"] startIndex: ", startIndex, "Number of values", values.length)
      //console.error("["+ this.name +"] Maximal length of vector", this.length)
      throw(new Error("["+ this.name +"] Vector: too many values."))
    } else {
      for(let i=0; i<values.length;i++) {
        this.arr[startIndex+i] = values[i]
      }
    }
  }

  eucDistance(vector) {
    //console.log("["+ this.name +"][eucDistance] Comparing:", this.arr, vector.arr)
    /* Return the euclidian distance with another Vector */
    // https://supunkavinda.blog/js-euclidean-distance
    return this.arr
        .map((x, i) => Math.abs( x - vector.arr[i] ) ** 2) // square the difference
        .reduce((sum, now) => sum + now) // sum
        ** (1/2)
  }

  hammingDistance(vector) {
    let dist = 0
    for(let i=0; i<this.arr.length; i++) {
      if(this.arr[i] != vector.arr[i]) {
        dist++
      }
    }
    return dist
  }
}

class TimestampVector extends Vector {
  eucDistance(vector) {
    return this.arr
        .map((x, i) => {
          if(x == 0 || vector.arr[i] == 0) {
            // ignoring the entry if there is only one (couple of) gateway(s)
            return 0
          } else {
            return Math.abs( x - vector.arr[i] ) ** 2
          }
        }) // square the difference
        .reduce((sum, now) => sum + now) // sum
        ** (1/2)
  }
}


module.exports = {
  combineGw,
  getCombinedValues,
  Vector,
  TimestampVector
}
