#!/usr/bin/env python3
import sys
import logging

# required before other imports (yes; see: https://stackoverflow.com/a/20280587)
logging.basicConfig(
    format='%(message)s',
    level=logging.DEBUG, # use logging.INFO to only print LaTeX
    stream=sys.stdout
)

import argparse

import numpy as np
from os import path
import pandas as pd
from math import sqrt

from sklearn.model_selection import train_test_split
from sklearn import metrics
from sklearn.model_selection import cross_validate
from sklearn.model_selection import RepeatedStratifiedKFold

from sklearn.ensemble import RandomForestClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.ensemble import HistGradientBoostingClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import AdaBoostClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.naive_bayes import GaussianNB

from joblib import dump, load

columns = None 

def get_features_from_csv(filename, excluded_columns=None, nrows=None):
    logging.debug(f"[-] Getting features from the following csv: {filename}")
    if(excluded_columns):
        logging.debug(f"[-] Excluding the following columns: '{excluded_columns}'")
        # https://stackoverflow.com/questions/48899051/how-to-drop-a-specific-column-of-csv-file-while-reading-it-using-pandas/48899141#48899141
        cols = list(pd.read_csv(filename, nrows =1))
        features = pd.read_csv(
            filename,
            usecols =[i for i in cols if i not in excluded_columns],
            dtype={'dutycycleDiff': 'O'},
            nrows=nrows,
        )
    else:
        features = pd.read_csv(
            filename,
            dtype={'dutycycleDiff': 'O'},
            nrows=nrows,
        )
    return features

###################
# Custom scorers! #
###################
def mcc(tp, fp, tn, fn):
    if(tp+fp == 0):
        return 0
    return (tp * tn - fp * fn) / sqrt((tp+fp)*(tp+fn)*(tn+fp)*(tn+fn))

def mcc_score(y_true, y_pred):
    tp = true_positives(y_true, y_pred)
    fp = false_positives(y_true, y_pred)
    tn = true_negatives(y_true, y_pred)
    fn = false_negatives(y_true, y_pred)
    return mcc(tp, fp, tn, fn)

def true_positives(y_true, y_pred):
    tp = ((y_pred == 1) & (y_true == 1)).sum()
    return tp

def false_positives(y_true, y_pred):
    fp = ((y_pred == 1) & (y_true == 0)).sum()
    return fp

def true_negatives(y_true, y_pred):
    tn = ((y_pred == 0) & (y_true == 0)).sum()
    return tn

def false_negatives(y_true, y_pred):
    fn = ((y_pred == 0) & (y_true == 1)).sum()
    return fn


class Model(object):
    """docstring for Model."""

    def __init__(self, name, dir, clf, train_features, test_features, train_labels, test_labels, cv, scoring):
        """
        clf: classifier already instanciated
        """
        super(Model, self).__init__()
        self.name = name
        self.dir = dir

        # using two different models just to be sure which one we're using
        self.clf = clf
        self.clf_fit = None

        self.train_features = train_features
        self.test_features = test_features
        self.train_labels = train_labels
        self.test_labels = test_labels
        self.cv = cv
        self.scoring = scoring
        logging.debug(f"-----------------------------")
        logging.debug(f"\n[+] New model: {self.name}")

    def start(self):
        self.train_and_validate()
        self.save()

        self.display_cv()
        self.display_weights()

        self.test()
        self.display_test()

    def save(self):
        filename = f"{self.name.replace(' ', '')}.joblib"
        p = path.join(self.dir, filename)
        logging.debug(f"[{self.name}] Saving the fit model to: {p}")
        dump(self.clf_fit, p)

    def display_results(self, tp, fp, tn, fn):
        f1 = round(tp / (tp + (0.5 * (fp + fn))), 4)
        m = round(mcc(tp, fp, tn, fn), 4)
        prec = round(tp / (tp + fp), 4)
        tpr = round(tp / (tp+fn), 4)
        fpr = round(fp / (fp+tn), 4)

        logging.debug(f"[{self.name}] True positives: {tp}")
        logging.debug(f"[{self.name}] False positives: {fp}")
        logging.debug(f"[{self.name}] True negatives: {tn}")
        logging.debug(f"[{self.name}] False negatives: {fn}")
        logging.debug(f"[{self.name}] True positive rate {tpr}")
        logging.debug(f"[{self.name}] False positive rate {fpr}")
        logging.debug(f"[{self.name}] Precision: {prec}")
        logging.debug(f"[{self.name}] F1-score: {f1}")
        logging.debug(f"[{self.name}] MCC: {m}")

        logging.debug("-- LaTeX -- ")
        line = f"{self.name} & {tp} & {fp} & {tn} & {fn} & {tpr} & {fpr} & {m} \\\\"
        logging.info(line)
        logging.debug("-----")

    def display_weights(self):
        logging.debug(f"[{self.name}] [weights]")
        if hasattr(self.clf_fit, 'coef_'):
            # returns a matrix of weights (coefficients)
            logging.debug(f"[{self.name}] coef_: {self.clf_fit.coef_}")
        elif hasattr(self.clf_fit, "feature_importances_"):
            #  RF
            logging.debug(f"[{self.name}] feature_importances_: {self.clf_fit.feature_importances_}")
            for w, f in sorted(zip(self.clf_fit.feature_importances_, columns), reverse=True):
                logging.info(f"{f} & {round(w, 4)} \\\\")
        elif hasattr(self.clf_fit, "effective_metric_"):
            # kNN
            logging.debug(f"[{self.name}] effective_metric_: {self.clf_fit.effective_metric_}")

    def train_and_validate(self):
        logging.debug(f"[{self.name}] Cross Validation")
        # Cross validation
        self.cv_results = cross_validate(
            self.clf,
            self.train_features,
            self.train_labels,
            cv=self.cv,
            scoring=self.scoring,
            return_estimator=True
        )
        # Selecting the model with the best MCC for later use
        best = max(self.cv_results['test_mcc'])
        i = self.cv_results['test_mcc'].tolist().index(best)
        self.clf_fit = self.cv_results['estimator'][i]

    def display_cv(self):
        results = self.cv_results
        tp = np.mean(results['test_tp'])
        fp = np.mean(results['test_fp'])
        tn = np.mean(results['test_tn'])
        fn = np.mean(results['test_fn'])

        logging.debug(f"[{self.name}] [cv]")
        self.display_results(tp, fp, tn, fn)

    def test(self):
        # performing predictions on the test dataset
        logging.debug(f"[{self.name}] Prediction...")
        self.test_pred = self.clf_fit.predict(self.test_features)

    def display_test(self):
        # as this is a binary classifying problem, the confusion matrix gives us
        # directly the true/false positives/negatives
        tn, fp, fn, tp = metrics.confusion_matrix(self.test_labels, self.test_pred).ravel()
        logging.debug(f"[{self.name}] [test]")
        self.display_results(tp, fp, tn, fn)


if __name__ == '__main__':
    logging.debug("Let's go :)")

    parser = argparse.ArgumentParser(description="Use various classifiers to play with logs.")
    parser.add_argument('--inputfile', '-i', help='The csv file to use as input data')
    parser.add_argument('--exclude', '-e', default="", help='Comma separated columns to exclude')
    parser.add_argument('--nrows', '-n', default=None, help='Number of lines to use from the CSV')
    parser.add_argument('--random', '-r', default=42, help='Random state (default: 42)')

    args = parser.parse_args()

    random_state = args.random
    excluded_columns = args.exclude.split(',')
    nrows = None
    if(args.nrows):
        nrows = int(args.nrows)

    ########

    # Getting data from the csv file
    all_data = get_features_from_csv(
        args.inputfile,
        excluded_columns=excluded_columns,
        nrows=nrows,
    )

    labels = np.array(all_data['linked'])
    features = all_data.drop('linked', axis=1) # axis 1 refers to the columns
    columns = features.columns
    features = np.array(features)

    # 75% training dataset, 25% test dataset
    train_features, test_features, train_labels, test_labels = train_test_split(
        features,
        labels,
        test_size=0.25,
        random_state=random_state
    )

    logging.debug(f"Training Features Shape: {train_features.shape}")
    logging.debug(f"Training Labels Shape: {train_labels.shape}")
    logging.debug(f"Testing Features Shape: {test_features.shape}")
    logging.debug(f"Testing Labels Shape: {test_labels.shape}")

    # Custom scorers
    scoring = {
        'mcc': metrics.make_scorer(mcc_score),
        'tp': metrics.make_scorer(true_positives),
        'fp': metrics.make_scorer(false_positives),
        'tn': metrics.make_scorer(true_negatives),
        'fn': metrics.make_scorer(false_negatives)
    }

    # Using strafified because imbalanced data and repeated for smoothing
    cv = RepeatedStratifiedKFold(
        n_splits=5,
        n_repeats=5,
        random_state=random_state
    )

    ###############
    # Classifiers #
    ###############
    logging.debug("[-] Creating classifiers.")
    names = [
        "RF",
        "kNN",
        "LGBM",
        "DT",
        "LR",
        "AB",
        "NB",
    ]

    classifiers = [
        RandomForestClassifier(random_state = random_state, n_estimators = 100), # default
        KNeighborsClassifier(n_neighbors = 5), # default
        HistGradientBoostingClassifier(random_state = random_state),
        DecisionTreeClassifier(random_state = random_state),
        LogisticRegression(random_state = random_state),
        AdaBoostClassifier(random_state = random_state),
        GaussianNB(),
    ]

    for name, clf in zip(names, classifiers):
        model = Model(name,
            "../../data/ml/models/",
            clf,
            train_features, test_features, train_labels, test_labels,
            cv, scoring)
        model.start()
